package com.bank;

public class LoanDepartment {
	
	private Long loanAccNo;
	private String name;
	private String pan;
	private Long loanAmount;
	private Long accNo;
	private int interestRate;
	public LoanDepartment() {
		super();
	}
	public LoanDepartment(Long loanAccNo, String name, String pan, Long loanAmount, Long accNo, int interestRate) {
		super();
		this.loanAccNo = loanAccNo;
		this.name = name;
		this.pan = pan;
		this.loanAmount = loanAmount;
		this.accNo = accNo;
		this.interestRate = interestRate;
	}
	
	public Long totalBill(Long period) {
		return loanAmount + (loanAmount*interestRate*period)/100;
	}
	
	
	
	public Long getLoanAccNo() {
		return loanAccNo;
	}
	public void setLoanAccNo(Long loanAccNo) {
		this.loanAccNo = loanAccNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public Long getLoanAmount() {
		return loanAmount;
	}
	public void setLoanAmount(Long loanAmount) {
		this.loanAmount = loanAmount;
	}
	public Long getAccNo() {
		return accNo;
	}
	public void setAccNo(Long accNo) {
		this.accNo = accNo;
	}
	public int getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(int interestRate) {
		this.interestRate = interestRate;
	}	

}
